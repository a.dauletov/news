package kz.aslan.testworknews.base

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

abstract class BaseAdapter<T> : ListAdapter<T, BaseViewHolder<T>>(DiffCallback<T>()) {

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) = holder.bind(currentList[position])

    override fun getItemCount(): Int = currentList.size

    override fun onViewRecycled(holder: BaseViewHolder<T>) {
        super.onViewRecycled(holder)
        holder.onViewRecycled()
    }

    private class DiffCallback<T> : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T): Boolean =
            oldItem.toString() == newItem.toString()

        override fun areContentsTheSame(oldItem: T, newItem: T): Boolean =
            oldItem == newItem
    }
}