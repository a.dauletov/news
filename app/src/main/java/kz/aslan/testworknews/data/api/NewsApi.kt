package kz.aslan.testworknews.data.api

import kz.aslan.testworknews.BuildConfig
import kz.aslan.testworknews.domain.model.NewsInfo
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET("v2/top-headlines")
    suspend fun getTopHeadlines(
        @Query("q") keyword: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int,
        @Query("category") category: String = "general",
        @Query("apiKey") apiKey: String = BuildConfig.NEWS_API_KEY
    ): NewsInfo

    @GET("v2/everything")
    suspend fun getEverything(
        @Query("q") keyword: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int,
        @Query("apiKey") apiKey: String = BuildConfig.NEWS_API_KEY
    ): NewsInfo
}