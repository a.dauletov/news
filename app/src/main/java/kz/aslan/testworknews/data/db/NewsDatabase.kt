package kz.aslan.testworknews.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

private const val DATABASE_VERSION = 1

@Database(entities = [NewsEntity::class], version = DATABASE_VERSION)
abstract class NewsDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}