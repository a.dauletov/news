package kz.aslan.testworknews.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface NewsDao {

    @Query("SELECT * FROM News")
    suspend fun getAllNews(): List<NewsEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun saveNewsToDatabase(news: NewsEntity)

    @Delete
    suspend fun deleteNewsFromDatabase(news: NewsEntity)
}