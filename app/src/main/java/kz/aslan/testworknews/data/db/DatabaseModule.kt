package kz.aslan.testworknews.data.db

import androidx.room.Room
import kz.aslan.testworknews.base.InjectionModule
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module

private const val DATABASE_NAME = "news"

object DatabaseModule : InjectionModule {

    override fun create(): Module = module {
        single {
            Room
                .databaseBuilder(androidApplication(), NewsDatabase::class.java, DATABASE_NAME)
                .build()
        } bind NewsDatabase::class

        single { get<NewsDatabase>().newsDao() } bind NewsDao::class
    }
}