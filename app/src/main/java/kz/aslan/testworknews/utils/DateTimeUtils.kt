package kz.aslan.testworknews.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

private const val PATTERN_FULL_DATE_TIME = "dd.MM.yyyy HH:mm"

object DateTimeUtils {

    fun getFormattedDateTime(date: Date): String =
        SimpleDateFormat(PATTERN_FULL_DATE_TIME, Locale.getDefault()).format(date.time)
}