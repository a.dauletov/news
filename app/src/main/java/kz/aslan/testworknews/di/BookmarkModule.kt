package kz.aslan.testworknews.di

import kz.aslan.testworknews.base.InjectionModule
import kz.aslan.testworknews.view.bookmark.BookmarkViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object BookmarkModule : InjectionModule {

    override fun create(): Module = module {
        viewModel { BookmarkViewModel(get(), get()) }
    }
}