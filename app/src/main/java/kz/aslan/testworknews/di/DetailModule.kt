package kz.aslan.testworknews.di

import kz.aslan.testworknews.base.InjectionModule
import kz.aslan.testworknews.domain.usecases.DeleteNewsFromDatabaseUseCase
import kz.aslan.testworknews.domain.usecases.GetAllNewsFromDatabaseUseCase
import kz.aslan.testworknews.domain.usecases.SaveNewsToDatabaseUseCase
import kz.aslan.testworknews.view.detail.DetailViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object DetailModule : InjectionModule {

    override fun create(): Module = module {
        single { SaveNewsToDatabaseUseCase(get()) }
        single { GetAllNewsFromDatabaseUseCase(get()) }
        single { DeleteNewsFromDatabaseUseCase(get()) }

        viewModel { DetailViewModel(get(), get(), get()) }
    }
}