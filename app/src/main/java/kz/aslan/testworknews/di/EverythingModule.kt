package kz.aslan.testworknews.di

import kz.aslan.testworknews.base.InjectionModule
import kz.aslan.testworknews.domain.usecases.GetEverythingUseCase
import kz.aslan.testworknews.view.everything.EverythingViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object EverythingModule : InjectionModule {

    override fun create(): Module = module {
        single { GetEverythingUseCase(get()) }

        viewModel { EverythingViewModel(get(), get()) }
    }
}