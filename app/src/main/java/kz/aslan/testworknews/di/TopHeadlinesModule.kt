package kz.aslan.testworknews.di

import kz.aslan.testworknews.base.InjectionModule
import kz.aslan.testworknews.domain.usecases.GetTopHeadlinesUseCase
import kz.aslan.testworknews.view.topheadlines.TopHeadlinesViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object TopHeadlinesModule : InjectionModule {

    override fun create(): Module = module {
        single { GetTopHeadlinesUseCase(get()) }

        viewModel { TopHeadlinesViewModel(get(), get()) }
    }
}