package kz.aslan.testworknews.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kz.aslan.testworknews.BuildConfig
import kz.aslan.testworknews.base.DefaultLogger
import kz.aslan.testworknews.base.InjectionModule
import kz.aslan.testworknews.base.Logger
import kz.aslan.testworknews.data.api.NewsApi
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

private const val DEFAULT_CONNECT_TIMEOUT_SECONDS = 30L
private const val DEFAULT_READ_TIMEOUT_SECONDS = 30L
private const val DEFAULT_DISK_CACHE_SIZE = 256 * 1024 * 1024L

object CommonModule : InjectionModule {

    override fun create(): Module = module {
        single<Logger> { DefaultLogger(BuildConfig.DEBUG) }

        single {
            GsonBuilder()
                .apply { if (BuildConfig.DEBUG) setPrettyPrinting() }
                .create()
        } bind Gson::class

        single {
            createOkHttpClient(get())
                .apply { if (BuildConfig.DEBUG) addLoggingInterceptor(get()) }
                .build()
        } bind OkHttpClient::class

        single {
            Retrofit.Builder()
                .baseUrl(BuildConfig.NEWS_API_URL)
                .addConverterFactory(GsonConverterFactory.create(get()))
                .callFactory(get<OkHttpClient>())
                .build()
        } bind Retrofit::class

        single { get<Retrofit>().create(NewsApi::class.java) } bind NewsApi::class
    }

    private fun createOkHttpClient(context: Context): OkHttpClient.Builder =
        OkHttpClient.Builder()
            .readTimeout(DEFAULT_READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(DEFAULT_CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .cache(Cache(context.cacheDir, DEFAULT_DISK_CACHE_SIZE))
            .hostnameVerifier { _, _ -> true }

    private fun OkHttpClient.Builder.addLoggingInterceptor(gson: Gson): OkHttpClient.Builder {
        val okHttpLogTag = "OkHttp"

        val okHttpLogger = object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                if (!message.startsWith('{') && !message.startsWith('[')) {
                    Timber.tag(okHttpLogTag).d(message)
                    return
                }

                try {
                    val json = JsonParser.parseString(message)
                    Timber.tag(okHttpLogTag).d(gson.toJson(json))
                } catch (e: Exception) {
                    Timber.tag(okHttpLogTag).e(e)
                }
            }
        }

        val interceptor = HttpLoggingInterceptor(okHttpLogger).apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        return addInterceptor(interceptor)
    }
}