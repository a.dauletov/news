package kz.aslan.testworknews

import android.app.Application
import kz.aslan.testworknews.data.db.DatabaseModule
import kz.aslan.testworknews.di.BookmarkModule
import kz.aslan.testworknews.di.CommonModule
import kz.aslan.testworknews.di.DetailModule
import kz.aslan.testworknews.di.EverythingModule
import kz.aslan.testworknews.di.TopHeadlinesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(
                CommonModule.create(),
                DatabaseModule.create(),
                TopHeadlinesModule.create(),
                EverythingModule.create(),
                DetailModule.create(),
                BookmarkModule.create()
            )
        }
    }
}