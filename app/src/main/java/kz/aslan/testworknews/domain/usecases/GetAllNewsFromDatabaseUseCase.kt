package kz.aslan.testworknews.domain.usecases

import kz.aslan.testworknews.data.db.NewsDao
import kz.aslan.testworknews.domain.model.ArticleInfo
import kz.aslan.testworknews.domain.model.SourceInfo
import timber.log.Timber
import java.util.Date

class GetAllNewsFromDatabaseUseCase(
    private val newsDao: NewsDao
) {

    suspend fun invoke(): List<ArticleInfo> {
        val news = newsDao.getAllNews()
        Timber.tag("Aslan").d("News: ${news.size}")
        return news.map { entity ->
            ArticleInfo(
                SourceInfo(name = entity.source),
                entity.author,
                entity.title,
                entity.description,
                entity.url,
                entity.urlToImage,
                entity.publishedAt?.let { Date(it) },
                entity.content
            )
        }
    }
}