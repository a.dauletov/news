package kz.aslan.testworknews.domain.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SourceInfo(
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("name")
    val name: String? = null
) : Serializable
