package kz.aslan.testworknews.domain.usecases

import kz.aslan.testworknews.data.db.NewsDao
import kz.aslan.testworknews.data.db.NewsEntity
import kz.aslan.testworknews.domain.model.ArticleInfo

class SaveNewsToDatabaseUseCase(
    private val newsDao: NewsDao
) {

    suspend fun invoke(article: ArticleInfo) =
        newsDao.saveNewsToDatabase(
            NewsEntity(
                source = article.source.name,
                author = article.author,
                title = article.title,
                description = article.description,
                content = article.content,
                url = article.url,
                urlToImage = article.urlToImage,
                publishedAt = article.publishedAt?.time
            )
        )
}