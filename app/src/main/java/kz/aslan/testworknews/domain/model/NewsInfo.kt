package kz.aslan.testworknews.domain.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class NewsInfo(
    @SerializedName("status")
    val status: String,
    @SerializedName("code")
    val errorCode: String? = null,
    @SerializedName("message")
    val errorMessage: String? = null,
    @SerializedName("totalResults")
    val totalResults: Int? = null,
    @SerializedName("articles")
    val articles: List<ArticleInfo>
) : Serializable
