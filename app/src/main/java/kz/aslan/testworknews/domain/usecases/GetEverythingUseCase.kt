package kz.aslan.testworknews.domain.usecases

import kz.aslan.testworknews.data.api.NewsApi

private const val PAGE_SIZE = 15

class GetEverythingUseCase(
    private val newsApi: NewsApi
) {

    suspend fun invoke(keyword: String, page: Int) =
        newsApi.getEverything(
            keyword = keyword,
            pageSize = PAGE_SIZE,
            page = page
        )
}