package kz.aslan.testworknews.domain.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date

data class ArticleInfo(
    @SerializedName("source")
    val source: SourceInfo,
    @SerializedName("author")
    val author: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("urlToImage")
    val urlToImage: String? = null,
    @SerializedName("publishedAt")
    val publishedAt: Date? = null,
    @SerializedName("content")
    val content: String? = null
) : Serializable
