package kz.aslan.testworknews.view.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import kz.aslan.testworknews.view.everything.EverythingFragment
import kz.aslan.testworknews.view.topheadlines.TopHeadlinesFragment

class MainPagerAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun createFragment(position: Int): Fragment = when (position) {
        0 -> TopHeadlinesFragment()
        else -> EverythingFragment()
    }

    override fun getItemCount(): Int = 2
}