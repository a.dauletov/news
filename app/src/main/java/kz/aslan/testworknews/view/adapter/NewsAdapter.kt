package kz.aslan.testworknews.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import kz.aslan.testworknews.base.BaseAdapter
import kz.aslan.testworknews.base.BaseViewHolder
import kz.aslan.testworknews.databinding.ItemNewsBinding
import kz.aslan.testworknews.domain.model.ArticleInfo
import kz.aslan.testworknews.utils.DateTimeUtils

class NewsAdapter(
    private val onItemClicked: (ArticleInfo) -> Unit
) : BaseAdapter<ArticleInfo>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<ArticleInfo> =
        NewsItemViewHolder(
            ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            onItemClicked
        )

    private class NewsItemViewHolder(
        private val viewBinding: ItemNewsBinding,
        onItemClicked: (ArticleInfo) -> Unit
    ) : BaseViewHolder<ArticleInfo>(viewBinding.root, onItemClicked) {

        override fun onBind(item: ArticleInfo) {
            super.onBind(item)
            with(viewBinding) {
                titleTextView.text = item.title
                authorTextView.text = item.author
                descriptionTextView.text = item.description
                sourceTextView.text = item.source.name
                item.publishedAt?.let {
                    publishedAtTextView.text = DateTimeUtils.getFormattedDateTime(it)
                }
            }
        }
    }
}