package kz.aslan.testworknews.view.bookmark

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_top_headlines.swipeRefreshLayout
import kz.aslan.testworknews.R
import kz.aslan.testworknews.databinding.FragmentBookmarkBinding
import kz.aslan.testworknews.extensions.replaceFragment
import kz.aslan.testworknews.view.adapter.NewsAdapter
import kz.aslan.testworknews.view.detail.DetailFragment
import org.koin.android.viewmodel.ext.android.viewModel

class BookmarkFragment : Fragment(R.layout.fragment_bookmark) {

    private lateinit var binding: FragmentBookmarkBinding
    private val viewModel: BookmarkViewModel by viewModel()

    private val adapter: NewsAdapter by lazy {
        NewsAdapter {
            replaceFragment(DetailFragment.create(it))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentBookmarkBinding.bind(view)

        with(binding) {
            toolbar.setNavigationOnClickListener { activity?.onBackPressed() }

            bookmarkRecyclerView.layoutManager = LinearLayoutManager(requireContext())
            bookmarkRecyclerView.addItemDecoration(
                DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            )
            bookmarkRecyclerView.adapter = adapter
        }

        viewModel.viewState.observe(viewLifecycleOwner, { viewState ->
            swipeRefreshLayout.isRefreshing = viewState.isLoading
            adapter.submitList(viewState.articles)
        })
    }
}