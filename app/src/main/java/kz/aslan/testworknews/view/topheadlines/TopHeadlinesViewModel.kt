package kz.aslan.testworknews.view.topheadlines

import kz.aslan.testworknews.base.Action
import kz.aslan.testworknews.base.BaseViewModel
import kz.aslan.testworknews.base.Logger
import kz.aslan.testworknews.base.ViewState
import kz.aslan.testworknews.domain.model.ArticleInfo
import kz.aslan.testworknews.domain.usecases.GetTopHeadlinesUseCase

class TopHeadlinesViewModel(
    logger: Logger,
    private val getTopHeadlinesUseCase: GetTopHeadlinesUseCase
) : BaseViewModel<TopHeadlinesViewState, TopHeadlinesAction>(logger, TopHeadlinesViewState()) {

    private var page = 1
    private var isActive = true

    init {
        getNews()
    }

    override fun handleLoading(isLoading: Boolean) {
        super.handleLoading(isLoading)
        setState { copy(isLoading = isLoading) }
    }

    override fun handleGeneralError(e: Throwable) {
        super.handleGeneralError(e)
        sendAction(TopHeadlinesAction.ShowErrorSnackbar)
    }

    override fun onCleared() {
        super.onCleared()
        isActive = false
    }

    fun onPageEnded() {
        page++
        getNews()
    }

    private fun getNews() {
        launchSafe {
            val newsInfo = getTopHeadlinesUseCase.invoke(keyword = "Apple", page = page)
            setState {
                if (page == 1) {
                    copy(articles = newsInfo.articles)
                } else {
                    val newArticles = articles.toMutableList()
                    newArticles.addAll(newsInfo.articles)
                    copy(articles = newArticles)
                }
            }
        }
    }
}

data class TopHeadlinesViewState(
    val isLoading: Boolean = false,
    val articles: List<ArticleInfo> = emptyList()
) : ViewState

sealed class TopHeadlinesAction : Action {
    object ShowErrorSnackbar : TopHeadlinesAction()
}