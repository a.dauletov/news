package kz.aslan.testworknews.view.detail

import kz.aslan.testworknews.base.Action
import kz.aslan.testworknews.base.BaseViewModel
import kz.aslan.testworknews.base.Logger
import kz.aslan.testworknews.base.ViewState
import kz.aslan.testworknews.domain.model.ArticleInfo
import kz.aslan.testworknews.domain.usecases.DeleteNewsFromDatabaseUseCase
import kz.aslan.testworknews.domain.usecases.SaveNewsToDatabaseUseCase
import timber.log.Timber

class DetailViewModel(
    logger: Logger,
    private val saveNewsToDatabaseUseCase: SaveNewsToDatabaseUseCase,
    private val deleteNewsFromDatabaseUseCase: DeleteNewsFromDatabaseUseCase
) : BaseViewModel<DetailViewState, Action>(logger, DetailViewState()) {

    override fun handleLoading(isLoading: Boolean) {
        super.handleLoading(isLoading)
        setState { copy(isLoading = isLoading) }
    }

    fun onBookmarkPressed(article: ArticleInfo, isBookmarked: Boolean) {
        launchSafe {
            Timber.tag("Aslan").d("$isBookmarked")
            if (isBookmarked) {
                deleteNewsFromDatabaseUseCase.invoke(article)
            } else {
                saveNewsToDatabaseUseCase.invoke(article)
            }
        }
    }
}

data class DetailViewState(
    val isLoading: Boolean = false
) : ViewState