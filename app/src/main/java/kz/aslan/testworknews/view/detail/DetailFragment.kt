package kz.aslan.testworknews.view.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import kz.aslan.testworknews.R
import kz.aslan.testworknews.databinding.FragmentDetailBinding
import kz.aslan.testworknews.domain.model.ArticleInfo
import kz.aslan.testworknews.utils.DateTimeUtils
import kz.aslan.testworknews.utils.args
import kz.aslan.testworknews.utils.withArgs
import org.koin.android.viewmodel.ext.android.viewModel

private const val ARTICLE_EXTRA = "ARTICLE_EXTRA"

class DetailFragment : Fragment(R.layout.fragment_detail) {

    companion object {
        fun create(article: ArticleInfo) =
            DetailFragment().withArgs(ARTICLE_EXTRA to article)
    }

    private val article: ArticleInfo by args(ARTICLE_EXTRA)

    private lateinit var binding: FragmentDetailBinding
    private val viewModel: DetailViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailBinding.bind(view)

        with(binding) {
            toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
            toolbar.setOnMenuItemClickListener { item ->
                if (item.itemId == R.id.bookmark) {
                    viewModel.onBookmarkPressed(article, item.isChecked)
                    item.isChecked = !item.isChecked
                    item.setIcon(
                        if (item.isChecked) {
                            R.drawable.ic_bookmark
                        } else {
                            R.drawable.ic_bookmark_border
                        }
                    )
                }
                true
            }

            Glide
                .with(requireContext())
                .load(article.urlToImage)
                .placeholder(R.drawable.placeholder)
                .into(detailImageView)

            titleTextView.text = article.title
            authorTextView.text = article.author
            descriptionTextView.text = article.description
            sourceTextView.text = article.source.name
            article.publishedAt?.let {
                publishedAtTextView.text = DateTimeUtils.getFormattedDateTime(it)
            }
        }
    }
}