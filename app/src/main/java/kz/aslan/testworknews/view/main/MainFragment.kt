package kz.aslan.testworknews.view.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import kz.aslan.testworknews.R
import kz.aslan.testworknews.databinding.FragmentMainBinding
import kz.aslan.testworknews.extensions.replaceFragment
import kz.aslan.testworknews.view.bookmark.BookmarkFragment

class MainFragment : Fragment(R.layout.fragment_main) {

    private lateinit var binding: FragmentMainBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)

        val adapter = MainPagerAdapter(childFragmentManager, lifecycle)
        with(binding) {
            toolbar.setOnMenuItemClickListener {
                replaceFragment(BookmarkFragment())
                true
            }

            viewPager.adapter = adapter

            val tabs = resources.getStringArray(R.array.news_tabs)
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = tabs[position]
            }.attach()
        }
    }
}