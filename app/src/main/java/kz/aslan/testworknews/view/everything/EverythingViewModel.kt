package kz.aslan.testworknews.view.everything

import kz.aslan.testworknews.base.Action
import kz.aslan.testworknews.base.BaseViewModel
import kz.aslan.testworknews.base.Logger
import kz.aslan.testworknews.base.ViewState
import kz.aslan.testworknews.domain.model.ArticleInfo
import kz.aslan.testworknews.domain.usecases.GetEverythingUseCase

class EverythingViewModel(
    logger: Logger,
    private val getEverythingUseCase: GetEverythingUseCase
) : BaseViewModel<EverythingViewState, EverythingAction>(logger, EverythingViewState()) {

    init {
        getNews()
    }

    private var page = 1

    override fun handleLoading(isLoading: Boolean) {
        super.handleLoading(isLoading)
        setState { copy(isLoading = isLoading) }
    }

    override fun handleGeneralError(e: Throwable) {
        super.handleGeneralError(e)
        sendAction(EverythingAction.ShowErrorSnackbar)
    }

    fun onRefreshed() {
        page = 1
        getNews()
    }

    fun onPageEnded() {
        page++
        getNews()
    }

    private fun getNews() {
        launchSafe {
            val newsInfo = getEverythingUseCase.invoke(keyword = "Apple", page = page)
            setState {
                if (page == 1) {
                    copy(articles = newsInfo.articles)
                } else {
                    val newArticles = articles.toMutableList()
                    newArticles.addAll(newsInfo.articles)
                    copy(articles = newArticles)
                }
            }
        }
    }
}

data class EverythingViewState(
    val isLoading: Boolean = false,
    val articles: List<ArticleInfo> = emptyList()
) : ViewState

sealed class EverythingAction : Action {
    object ShowErrorSnackbar : EverythingAction()
}