package kz.aslan.testworknews.view.topheadlines

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_top_headlines.swipeRefreshLayout
import kz.aslan.testworknews.R
import kz.aslan.testworknews.databinding.FragmentTopHeadlinesBinding
import kz.aslan.testworknews.extensions.replaceFragment
import kz.aslan.testworknews.view.adapter.NewsAdapter
import kz.aslan.testworknews.view.detail.DetailFragment
import org.koin.android.viewmodel.ext.android.viewModel

private const val DIRECTION_DOWN = 1

class TopHeadlinesFragment : Fragment(R.layout.fragment_top_headlines) {

    private lateinit var binding: FragmentTopHeadlinesBinding
    private val viewModel: TopHeadlinesViewModel by viewModel()

    private val adapter: NewsAdapter by lazy {
        NewsAdapter { article ->
            replaceFragment(DetailFragment.create(article))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentTopHeadlinesBinding.bind(view)

        with(binding) {
            topHeadlinesRecyclerView.layoutManager = LinearLayoutManager(requireContext())
            topHeadlinesRecyclerView.addItemDecoration(
                DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            )
            topHeadlinesRecyclerView.adapter = adapter
            topHeadlinesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (!recyclerView.canScrollVertically(DIRECTION_DOWN)) {
                        viewModel.onPageEnded()
                    }
                }
            })
        }

        viewModel.viewState.observe(viewLifecycleOwner, { viewState ->
            swipeRefreshLayout.isRefreshing = viewState.isLoading
            adapter.submitList(viewState.articles)
        })

        viewModel.actions.observe(viewLifecycleOwner, { action ->
            when (action) {
                is TopHeadlinesAction.ShowErrorSnackbar -> showErrorSnackbar()
            }
        })
    }

    private fun showErrorSnackbar() {
        Snackbar.make(binding.root, R.string.error, Snackbar.LENGTH_SHORT).show()
    }
}