package kz.aslan.testworknews.view.bookmark

import kz.aslan.testworknews.base.Action
import kz.aslan.testworknews.base.BaseViewModel
import kz.aslan.testworknews.base.Logger
import kz.aslan.testworknews.base.ViewState
import kz.aslan.testworknews.domain.model.ArticleInfo
import kz.aslan.testworknews.domain.usecases.GetAllNewsFromDatabaseUseCase

class BookmarkViewModel(
    logger: Logger,
    private val getAllNewsFromDatabaseUseCase: GetAllNewsFromDatabaseUseCase
) : BaseViewModel<BookmarkViewState, Action>(logger, BookmarkViewState()) {

    init {
        launchSafe {
            val articles = getAllNewsFromDatabaseUseCase.invoke()

            setState { copy(articles = articles) }
        }
    }
}

data class BookmarkViewState(
    val isLoading: Boolean = false,
    val articles: List<ArticleInfo> = emptyList()
) : ViewState