package kz.aslan.testworknews

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kz.aslan.testworknews.databinding.ActivitySingleBinding
import kz.aslan.testworknews.extensions.replaceFragment
import kz.aslan.testworknews.view.main.MainFragment

class SingleActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySingleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySingleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        replaceFragment(MainFragment())
    }
}