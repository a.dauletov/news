/**
 * Automatically generated file. DO NOT MODIFY
 */
package kz.aslan.testworknews;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "kz.aslan.testworknews";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Field from build type: debug
  public static final String NEWS_API_KEY = "e65ee0938a2a43ebb15923b48faed18d";
  // Field from build type: debug
  public static final String NEWS_API_URL = "https://newsapi.org";
}
