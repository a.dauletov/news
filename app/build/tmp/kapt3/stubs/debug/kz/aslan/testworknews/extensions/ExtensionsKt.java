package kz.aslan.testworknews.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u001a0\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t\u00a8\u0006\u000b"}, d2 = {"replaceFragment", "", "Landroidx/fragment/app/Fragment;", "fragment", "container", "", "addToBackStack", "", "tag", "", "Landroidx/fragment/app/FragmentActivity;", "app_debug"})
public final class ExtensionsKt {
    
    public static final void replaceFragment(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity $this$replaceFragment, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment fragment, @org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
    }
    
    public static final void replaceFragment(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$replaceFragment, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment fragment, @androidx.annotation.IdRes()
    int container, boolean addToBackStack, @org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
    }
}