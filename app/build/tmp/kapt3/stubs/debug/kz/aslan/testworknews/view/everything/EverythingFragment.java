package kz.aslan.testworknews.view.everything;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\b\u0010\u0016\u001a\u00020\u0011H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\b\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0017"}, d2 = {"Lkz/aslan/testworknews/view/everything/EverythingFragment;", "Landroidx/fragment/app/Fragment;", "()V", "adapter", "Lkz/aslan/testworknews/view/adapter/NewsAdapter;", "getAdapter", "()Lkz/aslan/testworknews/view/adapter/NewsAdapter;", "adapter$delegate", "Lkotlin/Lazy;", "binding", "Lkz/aslan/testworknews/databinding/FragmentEverythingBinding;", "viewModel", "Lkz/aslan/testworknews/view/everything/EverythingViewModel;", "getViewModel", "()Lkz/aslan/testworknews/view/everything/EverythingViewModel;", "viewModel$delegate", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "showErrorSnackbar", "app_debug"})
public final class EverythingFragment extends androidx.fragment.app.Fragment {
    private kz.aslan.testworknews.databinding.FragmentEverythingBinding binding;
    private final kotlin.Lazy viewModel$delegate = null;
    private final kotlin.Lazy adapter$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    public EverythingFragment() {
        super();
    }
    
    private final kz.aslan.testworknews.view.everything.EverythingViewModel getViewModel() {
        return null;
    }
    
    private final kz.aslan.testworknews.view.adapter.NewsAdapter getAdapter() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void showErrorSnackbar() {
    }
}