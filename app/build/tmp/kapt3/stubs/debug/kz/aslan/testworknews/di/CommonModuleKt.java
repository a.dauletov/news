package kz.aslan.testworknews.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"}, d2 = {"DEFAULT_CONNECT_TIMEOUT_SECONDS", "", "DEFAULT_DISK_CACHE_SIZE", "DEFAULT_READ_TIMEOUT_SECONDS", "app_debug"})
public final class CommonModuleKt {
    private static final long DEFAULT_CONNECT_TIMEOUT_SECONDS = 30L;
    private static final long DEFAULT_READ_TIMEOUT_SECONDS = 30L;
    private static final long DEFAULT_DISK_CACHE_SIZE = 268435456L;
}