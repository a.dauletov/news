package kz.aslan.testworknews.domain.usecases;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J!\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\f"}, d2 = {"Lkz/aslan/testworknews/domain/usecases/GetEverythingUseCase;", "", "newsApi", "Lkz/aslan/testworknews/data/api/NewsApi;", "(Lkz/aslan/testworknews/data/api/NewsApi;)V", "invoke", "Lkz/aslan/testworknews/domain/model/NewsInfo;", "keyword", "", "page", "", "(Ljava/lang/String;ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class GetEverythingUseCase {
    private final kz.aslan.testworknews.data.api.NewsApi newsApi = null;
    
    public GetEverythingUseCase(@org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.data.api.NewsApi newsApi) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object invoke(@org.jetbrains.annotations.NotNull()
    java.lang.String keyword, int page, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kz.aslan.testworknews.domain.model.NewsInfo> continuation) {
        return null;
    }
}