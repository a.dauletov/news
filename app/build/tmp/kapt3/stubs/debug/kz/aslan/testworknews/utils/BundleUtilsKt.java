package kz.aslan.testworknews.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000.\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u001a:\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0006\b\u0000\u0010\u0003\u0018\u00012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u0001H\u0003H\u0086\b\u00a2\u0006\u0002\u0010\u0007\u001a8\u0010\b\u001a\u0002H\u0003\"\u0006\b\u0000\u0010\u0003\u0018\u00012\b\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u0001H\u0003H\u0086\b\u00a2\u0006\u0002\u0010\u000b\u001aI\u0010\f\u001a\u0002H\u0003\"\b\b\u0000\u0010\u0003*\u00020\u0002*\u0002H\u00032.\u0010\r\u001a\u0018\u0012\u0014\b\u0001\u0012\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u000f0\u000e\"\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u000f\u00a2\u0006\u0002\u0010\u0011\u00a8\u0006\u0012"}, d2 = {"args", "Lkotlin/properties/ReadWriteProperty;", "Landroidx/fragment/app/Fragment;", "T", "key", "", "defaultValue", "(Ljava/lang/String;Ljava/lang/Object;)Lkotlin/properties/ReadWriteProperty;", "extractFromBundle", "bundle", "Landroid/os/Bundle;", "(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;", "withArgs", "params", "", "Lkotlin/Pair;", "", "(Landroidx/fragment/app/Fragment;[Lkotlin/Pair;)Landroidx/fragment/app/Fragment;", "app_debug"})
public final class BundleUtilsKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends androidx.fragment.app.Fragment>T withArgs(@org.jetbrains.annotations.NotNull()
    T $this$withArgs, @org.jetbrains.annotations.NotNull()
    kotlin.Pair<java.lang.String, ? extends java.lang.Object>... params) {
        return null;
    }
}