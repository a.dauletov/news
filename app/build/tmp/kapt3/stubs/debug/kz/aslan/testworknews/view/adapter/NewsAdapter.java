package kz.aslan.testworknews.view.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\rB\u0019\u0012\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\u001e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lkz/aslan/testworknews/view/adapter/NewsAdapter;", "Lkz/aslan/testworknews/base/BaseAdapter;", "Lkz/aslan/testworknews/domain/model/ArticleInfo;", "onItemClicked", "Lkotlin/Function1;", "", "(Lkotlin/jvm/functions/Function1;)V", "onCreateViewHolder", "Lkz/aslan/testworknews/base/BaseViewHolder;", "parent", "Landroid/view/ViewGroup;", "viewType", "", "NewsItemViewHolder", "app_debug"})
public final class NewsAdapter extends kz.aslan.testworknews.base.BaseAdapter<kz.aslan.testworknews.domain.model.ArticleInfo> {
    private final kotlin.jvm.functions.Function1<kz.aslan.testworknews.domain.model.ArticleInfo, kotlin.Unit> onItemClicked = null;
    
    public NewsAdapter(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kz.aslan.testworknews.domain.model.ArticleInfo, kotlin.Unit> onItemClicked) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kz.aslan.testworknews.base.BaseViewHolder<kz.aslan.testworknews.domain.model.ArticleInfo> onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\u0010\u0010\t\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u0002H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lkz/aslan/testworknews/view/adapter/NewsAdapter$NewsItemViewHolder;", "Lkz/aslan/testworknews/base/BaseViewHolder;", "Lkz/aslan/testworknews/domain/model/ArticleInfo;", "viewBinding", "Lkz/aslan/testworknews/databinding/ItemNewsBinding;", "onItemClicked", "Lkotlin/Function1;", "", "(Lkz/aslan/testworknews/databinding/ItemNewsBinding;Lkotlin/jvm/functions/Function1;)V", "onBind", "item", "app_debug"})
    static final class NewsItemViewHolder extends kz.aslan.testworknews.base.BaseViewHolder<kz.aslan.testworknews.domain.model.ArticleInfo> {
        private final kz.aslan.testworknews.databinding.ItemNewsBinding viewBinding = null;
        
        public NewsItemViewHolder(@org.jetbrains.annotations.NotNull()
        kz.aslan.testworknews.databinding.ItemNewsBinding viewBinding, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super kz.aslan.testworknews.domain.model.ArticleInfo, kotlin.Unit> onItemClicked) {
            super(null, null);
        }
        
        @java.lang.Override()
        protected void onBind(@org.jetbrains.annotations.NotNull()
        kz.aslan.testworknews.domain.model.ArticleInfo item) {
        }
    }
}