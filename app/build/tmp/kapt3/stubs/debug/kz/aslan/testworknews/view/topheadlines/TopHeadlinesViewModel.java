package kz.aslan.testworknews.view.topheadlines;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\nH\u0014J\b\u0010\u0014\u001a\u00020\u000eH\u0014J\u0006\u0010\u0015\u001a\u00020\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lkz/aslan/testworknews/view/topheadlines/TopHeadlinesViewModel;", "Lkz/aslan/testworknews/base/BaseViewModel;", "Lkz/aslan/testworknews/view/topheadlines/TopHeadlinesViewState;", "Lkz/aslan/testworknews/view/topheadlines/TopHeadlinesAction;", "logger", "Lkz/aslan/testworknews/base/Logger;", "getTopHeadlinesUseCase", "Lkz/aslan/testworknews/domain/usecases/GetTopHeadlinesUseCase;", "(Lkz/aslan/testworknews/base/Logger;Lkz/aslan/testworknews/domain/usecases/GetTopHeadlinesUseCase;)V", "isActive", "", "page", "", "getNews", "", "handleGeneralError", "e", "", "handleLoading", "isLoading", "onCleared", "onPageEnded", "app_debug"})
public final class TopHeadlinesViewModel extends kz.aslan.testworknews.base.BaseViewModel<kz.aslan.testworknews.view.topheadlines.TopHeadlinesViewState, kz.aslan.testworknews.view.topheadlines.TopHeadlinesAction> {
    private final kz.aslan.testworknews.domain.usecases.GetTopHeadlinesUseCase getTopHeadlinesUseCase = null;
    private int page = 1;
    private boolean isActive = true;
    
    public TopHeadlinesViewModel(@org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.base.Logger logger, @org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.domain.usecases.GetTopHeadlinesUseCase getTopHeadlinesUseCase) {
        super(null, null);
    }
    
    @java.lang.Override()
    protected void handleLoading(boolean isLoading) {
    }
    
    @java.lang.Override()
    protected void handleGeneralError(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable e) {
    }
    
    @java.lang.Override()
    protected void onCleared() {
    }
    
    public final void onPageEnded() {
    }
    
    private final void getNews() {
    }
}