package kz.aslan.testworknews.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"}, d2 = {"Lkz/aslan/testworknews/di/EverythingModule;", "Lkz/aslan/testworknews/base/InjectionModule;", "()V", "create", "Lorg/koin/core/module/Module;", "app_debug"})
public final class EverythingModule implements kz.aslan.testworknews.base.InjectionModule {
    @org.jetbrains.annotations.NotNull()
    public static final kz.aslan.testworknews.di.EverythingModule INSTANCE = null;
    
    private EverythingModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.koin.core.module.Module create() {
        return null;
    }
}