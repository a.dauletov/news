package kz.aslan.testworknews.view.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0014J\u0016\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000eR\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lkz/aslan/testworknews/view/detail/DetailViewModel;", "Lkz/aslan/testworknews/base/BaseViewModel;", "Lkz/aslan/testworknews/view/detail/DetailViewState;", "Lkz/aslan/testworknews/base/Action;", "logger", "Lkz/aslan/testworknews/base/Logger;", "saveNewsToDatabaseUseCase", "Lkz/aslan/testworknews/domain/usecases/SaveNewsToDatabaseUseCase;", "deleteNewsFromDatabaseUseCase", "Lkz/aslan/testworknews/domain/usecases/DeleteNewsFromDatabaseUseCase;", "(Lkz/aslan/testworknews/base/Logger;Lkz/aslan/testworknews/domain/usecases/SaveNewsToDatabaseUseCase;Lkz/aslan/testworknews/domain/usecases/DeleteNewsFromDatabaseUseCase;)V", "handleLoading", "", "isLoading", "", "onBookmarkPressed", "article", "Lkz/aslan/testworknews/domain/model/ArticleInfo;", "isBookmarked", "app_debug"})
public final class DetailViewModel extends kz.aslan.testworknews.base.BaseViewModel<kz.aslan.testworknews.view.detail.DetailViewState, kz.aslan.testworknews.base.Action> {
    private final kz.aslan.testworknews.domain.usecases.SaveNewsToDatabaseUseCase saveNewsToDatabaseUseCase = null;
    private final kz.aslan.testworknews.domain.usecases.DeleteNewsFromDatabaseUseCase deleteNewsFromDatabaseUseCase = null;
    
    public DetailViewModel(@org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.base.Logger logger, @org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.domain.usecases.SaveNewsToDatabaseUseCase saveNewsToDatabaseUseCase, @org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.domain.usecases.DeleteNewsFromDatabaseUseCase deleteNewsFromDatabaseUseCase) {
        super(null, null);
    }
    
    @java.lang.Override()
    protected void handleLoading(boolean isLoading) {
    }
    
    public final void onBookmarkPressed(@org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.domain.model.ArticleInfo article, boolean isBookmarked) {
    }
}