package kz.aslan.testworknews.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"}, d2 = {"PATTERN_FULL_DATE_TIME", "", "app_debug"})
public final class DateTimeUtilsKt {
    private static final java.lang.String PATTERN_FULL_DATE_TIME = "dd.MM.yyyy HH:mm";
}