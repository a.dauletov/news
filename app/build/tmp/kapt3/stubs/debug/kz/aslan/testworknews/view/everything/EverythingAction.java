package kz.aslan.testworknews.view.everything;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0003B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0001\u0004\u00a8\u0006\u0005"}, d2 = {"Lkz/aslan/testworknews/view/everything/EverythingAction;", "Lkz/aslan/testworknews/base/Action;", "()V", "ShowErrorSnackbar", "Lkz/aslan/testworknews/view/everything/EverythingAction$ShowErrorSnackbar;", "app_debug"})
public abstract class EverythingAction implements kz.aslan.testworknews.base.Action {
    
    private EverythingAction() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lkz/aslan/testworknews/view/everything/EverythingAction$ShowErrorSnackbar;", "Lkz/aslan/testworknews/view/everything/EverythingAction;", "()V", "app_debug"})
    public static final class ShowErrorSnackbar extends kz.aslan.testworknews.view.everything.EverythingAction {
        @org.jetbrains.annotations.NotNull()
        public static final kz.aslan.testworknews.view.everything.EverythingAction.ShowErrorSnackbar INSTANCE = null;
        
        private ShowErrorSnackbar() {
            super();
        }
    }
}