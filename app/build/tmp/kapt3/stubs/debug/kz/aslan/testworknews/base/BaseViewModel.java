package kz.aslan.testworknews.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b&\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\b\b\u0001\u0010\u0003*\u00020\u00042\u00020\u00052\u00020\u0006B\u0015\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\nJ\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0014J\u0010\u0010%\u001a\u00020\"2\u0006\u0010&\u001a\u00020\'H\u0014J\u0015\u0010(\u001a\u00020\"2\u0006\u0010)\u001a\u00028\u0001H\u0004\u00a2\u0006\u0002\u0010*J!\u0010+\u001a\u00020\"2\u0017\u0010,\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000-\u00a2\u0006\u0002\b.H\u0004Jg\u0010/\u001a\u000200\"\u0004\b\u0002\u00101*\b\u0012\u0004\u0012\u0002H1022\u0014\b\u0002\u00103\u001a\u000e\u0012\u0004\u0012\u00020\'\u0012\u0004\u0012\u00020\"0-2\u0014\b\u0002\u00104\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\"0-2!\u00105\u001a\u001d\u0012\u0013\u0012\u0011H1\u00a2\u0006\f\b6\u0012\b\b7\u0012\u0004\b\b(8\u0012\u0004\u0012\u00020\"0-H\u0004J^\u00109\u001a\u000200*\u00020\u00062\u0014\b\u0002\u00103\u001a\u000e\u0012\u0004\u0012\u00020\'\u0012\u0004\u0012\u00020\"0-2\u0014\b\u0002\u00104\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\"0-2\u001c\u0010:\u001a\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00020\"0;\u0012\u0006\u0012\u0004\u0018\u00010<0-H\u0004\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010=R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00028\u00010\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0010\u0012\f\u0012\n \u0011*\u0004\u0018\u00018\u00008\u00000\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u00020\u0013X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u00028\u00008DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u001d\u001a\b\u0012\u0004\u0012\u00028\u00000\u001e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 \u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006>"}, d2 = {"Lkz/aslan/testworknews/base/BaseViewModel;", "S", "Lkz/aslan/testworknews/base/ViewState;", "A", "Lkz/aslan/testworknews/base/Action;", "Landroidx/lifecycle/ViewModel;", "Lkotlinx/coroutines/CoroutineScope;", "logger", "Lkz/aslan/testworknews/base/Logger;", "initialState", "(Lkz/aslan/testworknews/base/Logger;Lkz/aslan/testworknews/base/ViewState;)V", "actions", "Lkz/aslan/testworknews/base/SingleLiveEvent;", "getActions", "()Lkz/aslan/testworknews/base/SingleLiveEvent;", "atomicState", "Ljava/util/concurrent/atomic/AtomicReference;", "kotlin.jvm.PlatformType", "coroutineContext", "Lkotlin/coroutines/CoroutineContext;", "getCoroutineContext", "()Lkotlin/coroutines/CoroutineContext;", "loadingState", "Lkz/aslan/testworknews/base/ObservableLoadingCounter;", "state", "getState", "()Lkz/aslan/testworknews/base/ViewState;", "unexpectedErrorHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "viewState", "Landroidx/lifecycle/MutableLiveData;", "getViewState", "()Landroidx/lifecycle/MutableLiveData;", "handleGeneralError", "", "e", "", "handleLoading", "isLoading", "", "sendAction", "action", "(Lkz/aslan/testworknews/base/Action;)V", "setState", "reducer", "Lkotlin/Function1;", "Lkotlin/ExtensionFunctionType;", "launchFlowSafe", "Lkotlinx/coroutines/Job;", "T", "Lkotlinx/coroutines/flow/Flow;", "loading", "error", "onEach", "Lkotlin/ParameterName;", "name", "item", "launchSafe", "body", "Lkotlin/coroutines/Continuation;", "", "(Lkotlinx/coroutines/CoroutineScope;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/Job;", "app_debug"})
public abstract class BaseViewModel<S extends kz.aslan.testworknews.base.ViewState, A extends kz.aslan.testworknews.base.Action> extends androidx.lifecycle.ViewModel implements kotlinx.coroutines.CoroutineScope {
    private final kz.aslan.testworknews.base.Logger logger = null;
    private final kotlinx.coroutines.CoroutineExceptionHandler unexpectedErrorHandler = null;
    private final kz.aslan.testworknews.base.ObservableLoadingCounter loadingState = null;
    private final java.util.concurrent.atomic.AtomicReference<S> atomicState = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<S> viewState = null;
    @org.jetbrains.annotations.NotNull()
    private final kz.aslan.testworknews.base.SingleLiveEvent<A> actions = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.coroutines.CoroutineContext coroutineContext = null;
    
    public BaseViewModel(@org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.base.Logger logger, @org.jetbrains.annotations.NotNull()
    S initialState) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final S getState() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<S> getViewState() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kz.aslan.testworknews.base.SingleLiveEvent<A> getActions() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kotlin.coroutines.CoroutineContext getCoroutineContext() {
        return null;
    }
    
    protected void handleLoading(boolean isLoading) {
    }
    
    protected void handleGeneralError(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable e) {
    }
    
    protected final void setState(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super S, ? extends S> reducer) {
    }
    
    protected final void sendAction(@org.jetbrains.annotations.NotNull()
    A action) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final kotlinx.coroutines.Job launchSafe(@org.jetbrains.annotations.NotNull()
    kotlinx.coroutines.CoroutineScope $this$launchSafe, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> loading, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> error, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.coroutines.Continuation<? super kotlin.Unit>, ? extends java.lang.Object> body) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final <T extends java.lang.Object>kotlinx.coroutines.Job launchFlowSafe(@org.jetbrains.annotations.NotNull()
    kotlinx.coroutines.flow.Flow<? extends T> $this$launchFlowSafe, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> loading, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> error, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super T, kotlin.Unit> onEach) {
        return null;
    }
}