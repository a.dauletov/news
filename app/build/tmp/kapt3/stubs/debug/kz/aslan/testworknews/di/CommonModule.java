package kz.aslan.testworknews.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0014\u0010\t\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0002\u00a8\u0006\f"}, d2 = {"Lkz/aslan/testworknews/di/CommonModule;", "Lkz/aslan/testworknews/base/InjectionModule;", "()V", "create", "Lorg/koin/core/module/Module;", "createOkHttpClient", "Lokhttp3/OkHttpClient$Builder;", "context", "Landroid/content/Context;", "addLoggingInterceptor", "gson", "Lcom/google/gson/Gson;", "app_debug"})
public final class CommonModule implements kz.aslan.testworknews.base.InjectionModule {
    @org.jetbrains.annotations.NotNull()
    public static final kz.aslan.testworknews.di.CommonModule INSTANCE = null;
    
    private CommonModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.koin.core.module.Module create() {
        return null;
    }
    
    private final okhttp3.OkHttpClient.Builder createOkHttpClient(android.content.Context context) {
        return null;
    }
    
    private final okhttp3.OkHttpClient.Builder addLoggingInterceptor(okhttp3.OkHttpClient.Builder $this$addLoggingInterceptor, com.google.gson.Gson gson) {
        return null;
    }
}