package kz.aslan.testworknews.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u0000*\u0004\b\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0003:\u0001\u0011B#\u0012\u001c\u0010\u0004\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0006\u0012\u0004\u0012\u00028\u00010\u0005\u00a2\u0006\u0002\u0010\u0007J\"\u0010\n\u001a\u00028\u00012\u0006\u0010\u000b\u001a\u00028\u00002\n\u0010\f\u001a\u0006\u0012\u0002\b\u00030\u0006H\u0096\u0002\u00a2\u0006\u0002\u0010\rJ*\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00028\u00002\n\u0010\f\u001a\u0006\u0012\u0002\b\u00030\u00062\u0006\u0010\b\u001a\u00028\u0001H\u0096\u0002\u00a2\u0006\u0002\u0010\u0010R$\u0010\u0004\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0006\u0012\u0004\u0012\u00028\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lkz/aslan/testworknews/utils/BundleExtractorDelegate;", "R", "T", "Lkotlin/properties/ReadWriteProperty;", "initializer", "Lkotlin/Function2;", "Lkotlin/reflect/KProperty;", "(Lkotlin/jvm/functions/Function2;)V", "value", "", "getValue", "thisRef", "property", "(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;", "setValue", "", "(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V", "EMPTY", "app_debug"})
public final class BundleExtractorDelegate<R extends java.lang.Object, T extends java.lang.Object> implements kotlin.properties.ReadWriteProperty<R, T> {
    private final kotlin.jvm.functions.Function2<R, kotlin.reflect.KProperty<?>, T> initializer = null;
    private java.lang.Object value;
    
    public BundleExtractorDelegate(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super R, ? super kotlin.reflect.KProperty<?>, ? extends T> initializer) {
        super();
    }
    
    @java.lang.Override()
    public void setValue(R thisRef, @org.jetbrains.annotations.NotNull()
    kotlin.reflect.KProperty<?> property, T value) {
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    @java.lang.Override()
    public T getValue(R thisRef, @org.jetbrains.annotations.NotNull()
    kotlin.reflect.KProperty<?> property) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lkz/aslan/testworknews/utils/BundleExtractorDelegate$EMPTY;", "", "()V", "app_debug"})
    static final class EMPTY {
        @org.jetbrains.annotations.NotNull()
        public static final kz.aslan.testworknews.utils.BundleExtractorDelegate.EMPTY INSTANCE = null;
        
        private EMPTY() {
            super();
        }
    }
}