package kz.aslan.testworknews.view.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0018"}, d2 = {"Lkz/aslan/testworknews/view/detail/DetailFragment;", "Landroidx/fragment/app/Fragment;", "()V", "article", "Lkz/aslan/testworknews/domain/model/ArticleInfo;", "getArticle", "()Lkz/aslan/testworknews/domain/model/ArticleInfo;", "article$delegate", "Lkotlin/properties/ReadWriteProperty;", "binding", "Lkz/aslan/testworknews/databinding/FragmentDetailBinding;", "viewModel", "Lkz/aslan/testworknews/view/detail/DetailViewModel;", "getViewModel", "()Lkz/aslan/testworknews/view/detail/DetailViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_debug"})
public final class DetailFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public static final kz.aslan.testworknews.view.detail.DetailFragment.Companion Companion = null;
    private final kotlin.properties.ReadWriteProperty article$delegate = null;
    private kz.aslan.testworknews.databinding.FragmentDetailBinding binding;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    public DetailFragment() {
        super();
    }
    
    private final kz.aslan.testworknews.domain.model.ArticleInfo getArticle() {
        return null;
    }
    
    private final kz.aslan.testworknews.view.detail.DetailViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lkz/aslan/testworknews/view/detail/DetailFragment$Companion;", "", "()V", "create", "Lkz/aslan/testworknews/view/detail/DetailFragment;", "article", "Lkz/aslan/testworknews/domain/model/ArticleInfo;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kz.aslan.testworknews.view.detail.DetailFragment create(@org.jetbrains.annotations.NotNull()
        kz.aslan.testworknews.domain.model.ArticleInfo article) {
            return null;
        }
    }
}