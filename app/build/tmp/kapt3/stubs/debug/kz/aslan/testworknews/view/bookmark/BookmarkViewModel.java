package kz.aslan.testworknews.view.bookmark;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lkz/aslan/testworknews/view/bookmark/BookmarkViewModel;", "Lkz/aslan/testworknews/base/BaseViewModel;", "Lkz/aslan/testworknews/view/bookmark/BookmarkViewState;", "Lkz/aslan/testworknews/base/Action;", "logger", "Lkz/aslan/testworknews/base/Logger;", "getAllNewsFromDatabaseUseCase", "Lkz/aslan/testworknews/domain/usecases/GetAllNewsFromDatabaseUseCase;", "(Lkz/aslan/testworknews/base/Logger;Lkz/aslan/testworknews/domain/usecases/GetAllNewsFromDatabaseUseCase;)V", "app_debug"})
public final class BookmarkViewModel extends kz.aslan.testworknews.base.BaseViewModel<kz.aslan.testworknews.view.bookmark.BookmarkViewState, kz.aslan.testworknews.base.Action> {
    private final kz.aslan.testworknews.domain.usecases.GetAllNewsFromDatabaseUseCase getAllNewsFromDatabaseUseCase = null;
    
    public BookmarkViewModel(@org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.base.Logger logger, @org.jetbrains.annotations.NotNull()
    kz.aslan.testworknews.domain.usecases.GetAllNewsFromDatabaseUseCase getAllNewsFromDatabaseUseCase) {
        super(null, null);
    }
}